Submitting a Cluster Job
========

If you would like to view and edit an example PBS script, see the **example** directory.

There is also a script that you can run on your local machine, which will configure and submit jobs on the cluster for you.  
To get started using this method, try the following:

`chmod +x .sh`  
`./delagator`  


The first time you run delagator, it will configure itself with your input.  
After that, you can submit cluster jobs by following the prompts.  

An example run looks like this:  
![Example run](example.png)
